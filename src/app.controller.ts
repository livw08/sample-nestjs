import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { User } from './user.entity';

@Controller('user')
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  @Post()
  async create(@Body() dto: User) {
    return this.appService.createUser(dto);
  }

  @Get(':ids')
  async get(@Param('ids') ids: string) {
    return this.appService.getById(ids);
  }
}
