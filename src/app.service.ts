import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './user.entity';

@Injectable()
export class AppService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async createUser(dto: User) {
    return this.userModel.create(dto);
  }

  async getById(ids: string) {
    return this.userModel.findOne({ id: ids });
  }
}
