import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from './user.entity';

@Module({
  imports: [
    // Cấu hình kết nối tới cơ sở dữ liệu
    MongooseModule.forRoot('mongodb://localhost/nest'),

    // Cấu hình các model được sử dụng
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
